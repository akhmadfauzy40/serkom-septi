<?php

use App\Http\Controllers\ReservasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reservasi', function () {
    return view('reservasi');
});
Route::post('/reservasi/store', [ReservasiController::class, 'store']);

Route::get('/cek-reservasi', [ReservasiController::class, 'grafik']);
Route::post('/cek-reservasi', [ReservasiController::class, 'cek_reservasi']);
