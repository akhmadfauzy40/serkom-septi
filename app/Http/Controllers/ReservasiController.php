<?php

namespace App\Http\Controllers;

use App\Models\Reservasi;
use Illuminate\Http\Request;
use stdClass;

class ReservasiController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'gender' => 'required',
            'no_identitas' => 'required|min:16',
            'tipe' => 'required',
            'harga' => 'required',
            'tanggal' => 'required',
            'durasi' => 'required',
            'total' => 'required',
        ]);

        $data = new Reservasi();
        $data->nama = $request->nama;
        $data->gender = $request->gender;
        $data->no_identitas = $request->no_identitas;
        $data->tipe = $request->tipe;
        $data->harga = $request->harga;
        $data->tanggal = $request->tanggal;
        $data->durasi = $request->durasi;
        $data->breakfast = $request->breakfast;
        $data->discount = $request->discount;
        $data->total = $request->total;
        $data->save();

        return redirect('reservasi')->with('success', 'Data telah berhasil ditambahkan!');
    }

    public function grafik()
    {
        $grafik = new stdClass;
        $grafik->standar = Reservasi::where('tipe', 'Standar')->count();
        $grafik->deluxe = Reservasi::where('tipe', 'Deluxe')->count();
        $grafik->executive = Reservasi::where('tipe', 'Executive')->count();

        return view('cek-reservasi', ['grafik' => $grafik]);
    }

    public function cek_reservasi(Request $request)
    {
        $request->validate([
            'no_identitas' => 'required'
        ]);

        $data = Reservasi::where('no_identitas', $request->no_identitas)->latest()->first();

        $grafik = new stdClass();
        $grafik->standar = Reservasi::where('tipe', 'Standar')->count();
        $grafik->deluxe = Reservasi::where('tipe', 'Deluxe')->count();
        $grafik->executive = Reservasi::where('tipe', 'Executive')->count();

        if ($data) {
            return view('cek-reservasi', ['data' => $data, 'grafik' => $grafik]);
        } else {
            return view('cek-reservasi', ['error' => 'Data Tidak di Temukan', 'grafik' => $grafik]);
        }
    }
}
