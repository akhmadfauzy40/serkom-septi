## Tutor Instalasi Web

- Link video tutorial instalasi web [klik di sini](https://drive.google.com/file/d/1UwgRL-UkvD7bjJB0bGKVsru0jOOUNEZa/view?usp=sharing)

- Buka direktori tempat anda ingin menyimpan project, lalu buka git bash
- ketik perintah di bawah

```bash
git clone https://gitlab.com/akhmadfauzy40/serkom-septi.git
```

- masuk ke folder project dengan mengetik perintah di bawah ini pada terminal

```bash
cd serkom-septi
```

- lalu ketik perintah di bawah ini

```bash
composer install
```

- buka vscode dengan mengetik perintah dibawah ini

```bash
code .
```

- pada vscode, dalam folder project buat file baru dengan nama ".env" lalu copy isi dari file ".env.example" dan paste ke dalam ".env"
- ubah keterangan database (ubah value "DB_DATABASE") sesuai dengan yang di inginkan dan jangan lupa di save
- lalu jalankan perintah di bawah ini

```bash
php artisan key:generate
```

- lalu ketik perintah (sebelum mengetik perintah di bawah ini pastikan XAMPP sudah di nyalakan)

```bash
php artisan migrate
```

- setelah itu jalankan serve dengan mengetik perintah di bawah ini

```bash
php artisan serve
```

- kalau sudah terinstall nantinya untuk menjalankan projectnya tinggal buka terminal lalu ketik perintah di bawah ini

```bash
php artisan serve
```
