@extends('layouts.master')


@section('content')
    <div class="container-fluid">
        <!--  Row 1 -->
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            <center>JENIS KAMAR YANG TERSEDIA</center>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card">
                                    <img src="https://elsotel.id/wp-content/uploads/2022/02/standard-thumb-331x480.jpg"
                                        class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Standar Room</h5>
                                        <p class="card-text">Standard Room memberikan sentuhan modern yang cocok untuk
                                            berbagai kebutuhan seperti liburan, perjalanan bisnis atau staycation.
                                            Dilengkapi dengan jendela yang memberikan akses cahaya masuk dan juga berguna
                                            untuk Anda menikmati pemandangan.</p>
                                        <p class="card-text">Kamar ini dilengkapi dengan bed yang super nyamanberukuran
                                            besar 200cm x 200cm untuk tipe King dan 2 buah bed dengan ukuran 100cm x 200cm
                                            untuk tipe Twin.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <img src="https://elsotel.id/wp-content/uploads/2022/03/deluxe-thumb-331x480.jpg"
                                        class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Deluxe Room</h5>
                                        <p class="card-text">Deluxe Room memberikan tempat yang lebih luas untuk Anda
                                            beraktivitas. Tipe kamar ini tersedia dua pilihan:</p>
                                        <p class="card-text">
                                        <ol>
                                            <li>Tipe jendela besar yang menyuguhkan pemandangan kota dan indahnya Gunung
                                                Slamet. Kamar ini dilengkapi dengan bed yang super nyaman dan berukuran
                                                besar 200cm x 200cm</li>
                                            <li>Tipe jendela sedang untuk memberikan privasi lebih kepada Anda. dilengkapi
                                                dengan 2 buah bed berukuran 100cm x 200cm</li>
                                        </ol>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <img src="https://elsotel.id/wp-content/uploads/2022/02/familysuite-thumb-331x480.jpg"
                                        class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Executive Room</h5>
                                        <p class="card-text">Executive Room adalah pilihan kamar yang tepat untuk keluarga.
                                            Ruangan yang luas memungkinkan Anda melakukan berbagai aktivitas bersama
                                            keluarga.</p>
                                        <p class="card-text">Anda dan keluarga bisa menikmati pemandangan Gunung Slamet dan
                                            indahnya kota melalui jendela yang besar.</p>
                                        <p class="card-text">Kamar ini juga dilengkapi dengan fasilitas bathtub guna
                                            memberikan kepuasan lebih. Dua bed berukuran besar 200cm x 200cm membuat kamar
                                            ini dapat menampung hingga 4 orang.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-title">
                                            <center>DAFTAR HARGA DAN DETAIL FASILITAS</center>
                                        </div>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Jenis Kamar</th>
                                                    <th>Fasilitas</th>
                                                    <th>Harga per Malam</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Standar Room</td>
                                                    <td>Size 22m persegi, 43″ LED TV, Sofa Long Chair, Working Table, Safety Box, Mini Bar, Coffee & Tea Maker, Room Amenities & Slippers, Rain & Hand Shower</td>
                                                    <td>Rp.400.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Deluxe Room</td>
                                                    <td>Size 30m persegi, 43″ LED TV, Working Table, Safety Box, Mini Bar, Coffee & Tea maker, Room Amenities & Slippers, Rain & Hand Shower</td>
                                                    <td>Rp.800.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Executive Room</td>
                                                    <td>Size 61m persegi, LED TV 43″, Dining Table, Working Table, Safety Box, Mini Bar, Coffee & Tea Maker, Room Amenities & Slippers, Bath Tub, Rain & Hand Shower</td>
                                                    <td>Rp.1.300.000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="#" class="pe-1 text-primary text-decoration-underline">Septi</a></p>
        </div>
    </div>
@endsection
