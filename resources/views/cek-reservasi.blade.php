@extends('layouts.master')


@section('content')
    <div class="container-fluid">
        <!--  Row 1 -->
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <h1>CEK RESERVASI HOTEL</h1>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Petunjuk Melakukan Pengecekan Reservasi</h4>
                                <p>Silahkan masukkan nomor identitas anda di kolom yang di sediakan di bawah ini lalu klik
                                    tombol cari data
                                    untuk melihat data reservasi anda</p>
                                <form action="/cek-reservasi" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="no_identitas">Nomor Identitas</label>
                                        <input type="number" class="form-control" id="no_identitas"
                                            placeholder="Masukkan Nomor Identitas Anda (KTP)" name="no_identitas" required>
                                    </div>
                                    @if (isset($error))
                                        <div class="alert alert-danger">
                                            {{ $error }}
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary mr-2">Cari Data</button>
                                </form>
                                <h4 class="card-title mt-4">Grafik Pemesanan Jenis Kamar</h4>
                                <div class="mb-3">
                                    <canvas id="pieChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Reservasi</h4>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama"
                                        placeholder="{{ isset($data) ? $data->nama : '' }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="no_identitas">Nomor Identitas</label>
                                    <input type="text" class="form-control" id="no_identitas"
                                        placeholder="{{ isset($data) ? $data->no_identitas : '' }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <input type="text" class="form-control" id="gender"
                                        placeholder="{{ isset($data) ? $data->gender : '' }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="tipe">Tipe Kamar</label>
                                    <input type="text" class="form-control" id="tipe"
                                        placeholder="{{ isset($data) ? $data->tipe : '' }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="durasi">Durasi Menginap</label>
                                    <input type="text" class="form-control" id="durasi"
                                        placeholder="{{ isset($data) ? $data->durasi : '0' }} Hari" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Discount</label>
                                    <input type="text" class="form-control" id="discount"
                                        placeholder="{{ isset($data) ? $data->discount : '0' }} %" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="durasi">Total Pembayaran</label>
                                    <input type="text" class="form-control" id="durasi"
                                        placeholder="{{ isset($data) ? $data->total : '' }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="#"
                    class="pe-1 text-primary text-decoration-underline">Septi</a></p>
        </div>
    </div>
@endsection

@push('styles')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@endpush

@push('scripts')
    <script>
        var standarData = @json($grafik->standar);
        var deluxeData = @json($grafik->deluxe);
        var executiveData = @json($grafik->executive);
        // Data untuk grafik pie
        var pieData = {
            labels: ['Standar Room', 'Deluxe Room', 'Executive Room'],
            datasets: [{
                data: [standarData, deluxeData, executiveData], // Data nilai untuk masing-masing bagian
                backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'], // Warna untuk masing-masing bagian
            }],
        };

        // Pengaturan untuk grafik pie
        var pieOptions = {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Doughnut Chart Example',
            },
        };

        // Mendapatkan elemen canvas
        var pieCanvas = document.getElementById('pieChart');

        // Membuat instance grafik pie
        var pieChart = new Chart(pieCanvas, {
            type: 'pie',
            data: pieData,
            options: pieOptions,
        });
    </script>
@endpush
