@extends('layouts.master')


@section('content')
    <div class="container-fluid">
        <!--  Row 1 -->
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <h1>BOOKING HOTEL</h1>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Form Booking Kamar</h4>
                                <form class="forms-sample" action="/reservasi/store" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="nama">Nama Pemesan</label>
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            id="nama" placeholder="Masukkan Nama Anda" name="nama" required>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="gender" class="mb-2">Gender</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="gender_pria"
                                                value="Pria" required>
                                            <label class="form-check-label" for="gender_pria">
                                                Pria
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="gender_wanita"
                                                value="Wanita" required>
                                            <label class="form-check-label" for="gender_wanita">
                                                Wanita
                                            </label>
                                        </div>
                                        @error('gender')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="no_identitas">Nomor Identitas</label>
                                        <input type="number" class="form-control" id="no_identitas"
                                            placeholder="Masukkan Nomor Identitas Anda (KTP)" name="no_identitas"
                                            oninput="validateInput(this)" required>
                                        <div id="ident_error" class="invalid-feedback"></div>
                                        @error('no_identitas')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="tipe">Tipe Kamar</label>
                                        <select class="form-control @error('tipe') is-invalid @enderror" id="tipe"
                                            name="tipe" required onchange="updateHarga()">
                                            <option selected disabled>Silahkan Pilih Tipe Kamar</option>
                                            <option value="Standar">Standar</option>
                                            <option value="Deluxe">Deluxe</option>
                                            <option value="Executive">Executive</option>
                                        </select>
                                        @error('tipe')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="harga">Harga Kamar</label>
                                        <input type="text" class="form-control @error('harga') is-invalid @enderror"
                                            id="harga" placeholder="Harga akan muncul otomatis" name="harga" readonly
                                            required>
                                        @error('harga')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="tanggal">Tanggal Pesan</label>
                                        <input type="date" class="form-control @error('tanggal') is-invalid @enderror"
                                            id="tanggal" placeholder="dd/mm/yyyy" name="tanggal" required>
                                        @error('tanggal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="durasi">Durasi Menginap</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control @error('durasi') is-invalid @enderror"
                                                id="durasi" placeholder="Silahkan Masukkan Durasi Menginap"
                                                name="durasi" required oninput="validateNumberInput(this)">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Hari</span>
                                            </div>
                                            <div id="durasi_error" class="invalid-feedback"></div>
                                            @error('durasi')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="breakfast"
                                                    value="Ya">
                                                Termasuk Breakfast </label>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="discount">Discount</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="discount"
                                                placeholder="Discount akan muncul otomatis" name="discount" readonly
                                                required>
                                            <div class="input-group-append">
                                                <span class="input-group-text">%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="total">Total</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                            <input type="number" class="form-control" id="total"
                                                placeholder="Harga akan terisi otomatis" name="total" required readonly>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary mr-2" onclick="hitungTotal()">Hitung
                                        Total Bayar</button>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tentang Hotel Aston</h4>
                                <h5>Sejarah Berdiri</h5>
                                <p>Hotel pertama Elsotel berdiri pada tahun 2022 di Purwokerto, Banyumas.</p>
                                <p>Kami bertujuan untuk menjadi hotel ternama di seluruh penjuru negeri dengan mengutamakan kualitas dan servis.</p>
                                <h5>Visi</h5>
                                <p>Menjadi produk perhotelan pilihan utama yang dikenal secara nasional baik oleh para tamu, rekanan, maupun pesaing dengan standar pelayanan yang berkualitas tinggi serta mengikuti perkembangan dunia industri perhotelan.</p>
                                <h5>Misi</h5>
                                <p>Menempatkan kualitas pelayanan yang tulus pada tingkat tertinggi untuk memenuhi harapan dan kebutuhan para tamu. Menjadi tempat paling nyaman bagi para tamu dalam menyelenggarakan kegiatan dengan motivasi kinerja karyawan yang penuh inspirasi serta memberikan dampak finansial yang menguntungkan bagi seluruh stakeholder.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="#"
                    class="pe-1 text-primary text-decoration-underline">Septi</a></p>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function validateInput(input) {
            var errorDiv = document.getElementById('ident_error');

            if (input.value.length < 16) {
                errorDiv.innerText = 'Minimal 16 karakter.';
                input.classList.add('is-invalid');
            } else {
                errorDiv.innerText = '';
                input.classList.remove('is-invalid');
            }
        }

        function updateHarga() {
            var tipeKamar = document.getElementById('tipe').value;
            var hargaInput = document.getElementById('harga');

            switch (tipeKamar) {
                case 'Standar':
                    hargaInput.value = 400000;
                    break;
                case 'Deluxe':
                    hargaInput.value = 800000;
                    break;
                case 'Executive':
                    hargaInput.value = 1300000;
                    break;
                default:
                    hargaInput.value = '';
                    break;
            }
        }

        function validateNumberInput(input) {
            var errorDiv = document.getElementById('durasi_error');

            // Memeriksa apakah input mengandung huruf
            if (/[^0-9]/.test(input.value)) {
                errorDiv.innerText = 'Hanya input angka yang diperbolehkan.';
                input.classList.add('is-invalid');
            } else {
                errorDiv.innerText = '';
                input.classList.remove('is-invalid');
            }
        }

        function hitungTotal() {
            var hargaKamar = 0;
            var totalInput = document.getElementById('total');
            var checkbox = document.getElementsByName('breakfast')[0];
            var durasi = document.getElementById('durasi');
            var discount = document.getElementById('discount');

            // Mendapatkan harga kamar
            var tipeKamar = document.getElementById('tipe').value;
            switch (tipeKamar) {
                case 'Standar':
                    hargaKamar = 400000;
                    break;
                case 'Deluxe':
                    hargaKamar = 800000;
                    break;
                case 'Executive':
                    hargaKamar = 1300000;
                    break;
                default:
                    hargaKamar = 0;
                    break;
            }

            // Menambahkan harga breakfast jika checkbox tercentang
            if (checkbox.checked) {
                hargaKamar += 80000; // Harga breakfast
            }

            if (durasi.value > 2) {
                hargaKamar = hargaKamar * durasi.value;
                discount.value = '10';
                var diskon = hargaKamar * 10 / 100;
                hargaKamar -= diskon;
            } else {
                hargaKamar = hargaKamar * durasi.value;
                discount.value = '0';
            }

            totalInput.value = hargaKamar;
        }
    </script>
@endpush
