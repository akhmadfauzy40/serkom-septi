<nav class="sidebar-nav scroll-sidebar" data-simplebar="">
    <ul id="sidebarnav">
        <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Menu Utama</span>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="/" aria-expanded="false">
                <span>
                    <i class="ti ti-layout-dashboard"></i>
                </span>
                <span class="hide-menu">INFORMASI HOTEL</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="/reservasi" aria-expanded="false">
                <span>
                    <i class="ti ti-article"></i>
                </span>
                <span class="hide-menu">RESERVASI KAMAR HOTEL</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="/cek-reservasi" aria-expanded="false">
                <span>
                    <i class="ti ti-file-description"></i>
                </span>
                <span class="hide-menu">CEK RESERVASI</span>
            </a>
        </li>
    </ul>
</nav>
